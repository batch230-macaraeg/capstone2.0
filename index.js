const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

const userRoutes = require("./routes/userRoutes.js");
const productRoutes = require("./routes/productRoutes.js");

const app = express();

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));

app.use("/user", userRoutes);
// app.use("/product", productRoutes);

mongoose.connect("mongodb+srv://admin:admin@batch230.8tlrfzd.mongodb.net/macaraegE-Commerce?retryWrites=true&w=majority", {

 	useNewUrlParser: true,
 	useUnifiedTopology: true
 })

mongoose.connection.once("open", () => console.log("Now you are connected to Macaraeg E-Commerce - Mongo DB Atlas"));

app.listen(process.env.PORT || 4000, () =>
{
	console.log(`API is now online on port ${process.env.PORT || 4000}`)
});