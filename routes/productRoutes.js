const express = require("express");
const router = express.Router();
const productController = require("../controllers/productController.js");
const auth = require("../auth.js");

// Creating NEW Product = OK
router.post("/create", (request, res) => {
	productController.addProduct(request.body).then(resultFromControllers => res.send(resultFromControllers))
})

// Retrieving ALL Product = OK
router.get("/allBooks", (request, response) => {
	productController.getAllProduct().then(resultFromControllers => response.send(resultFromControllers))
})

// Retrieving ACTIVE Product = OK
router.get("/activeBooks", (request, response) => {
	productController.getActiveProduct().then(resultFromControllers => response.send(resultFromControllers))
})

// Retrieving SINGLE Product = OK
router.get("/:productId", (request, response) => {
	productController.getProduct(request.params.productId).then(resultFromControllers => response.send(resultFromControllers));
})

// Updating Product = OK

router.patch("/:productId/update", auth.verify, (request,response) => 
{
 	const newData = {
 		product: request.body, 	
 		isAdmin: auth.decode(request.headers.authorization).isAdmin
 	}

 	productController.updateProduct(request.params.productId, newData)
 	.then(resultFromControllers => {
	response.send(resultFromControllers)
 	})
})


// Archiving Product = NOT OK

router.patch("/archive/:productId", auth.verify, productController.archiveProduct);

/*
 router.post("/create", auth.verify, (request, response) => 
 {
 		const result = {
 			product: request.body, 	
 			isAdmin: auth.decode(request.headers.authorization).isAdmin
 		}
 	productController.addProduct(request.body, result).then(resultFromControllers => response.send(resultFromControllers));
 })
*/

module.exports = router;