const jwt = require("jsonwebtoken");
const secret = "macaraegE-Commerce";

// Create Token
module.exports.createAccessToken = (user) => {
	// payload 
	const data = {
		email: user.email,
		password: user.password,
		isAdmin: user.isAdmin
	}
	return jwt.sign(data, secret, {/*expires:"60s*/});
}

/*
module.exports.verify = (request, response, next) => {
	// Get JWT (JSON Web Token) from postman
	let token = request.headers.authorization
	if(typeof token !== "undefined"){
		console.log(token);

		// removes the first characters ("Bearer ") from the token
		token = token.slice(7, token.length);

		return jwt.verify(token, secret, (error, data) =>{
			if(error){
				return response.send({
					auth: "Failed. "
				});
			}
			else{
				next();
			}
		})
	}
}
*/

// Middleware function
module.exports.verify = (req, res, next) =>{
	let token = req.headers.authorization;
	if (token !== "undefined"){		
		token = token.slice(7, token.length);
		console.log(token);

		return jwt.verify(token, secret, (err, data)=>{			
			if(err){
				return res.send({auth: "Invalid token!"});
			}			
			else{
				
				req.userData = data;
				console.log(req.userData)
				next();
			}
		})
	}
	else{
		res.send({message: "Auth failed. No token provided!"})
	}
}

// Token Decoder
// To decode the user details from the token
module.exports.decode = (token) => {

	if(typeof token !== true){

		// remove first 7 characters ("Bearer ") from the token
		token = token.slice(7, token.length);
	}

	return jwt.verify(token, secret, (error, data) => {
		if(error){
			return null
		}
		else{
			return jwt.decode(token, {complete:true}).payload
		}
	})
}
