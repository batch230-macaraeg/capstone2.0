const mongoose = require("mongoose");

const productSchema = new mongoose.Schema(
{
	name: {
		type: String,
		required: [true, "Product is required"]
	},
	description: {
		type: String,
		required: [true, "Description is required"]
	},
	price: {
		type: Number,
		required: [true, "Price is required"]
	},
	stocks: {
		type: Number,
		required: [true, "Stocks are required"]
	},
	isActive: {
		type: Boolean,
	 default: true
	},
	createdOn: {
		type: Date,
	 default: new Date()
	},

	orders: [
		{
			orderId: {
				type: String,
				required: [true, "Order Id is required"]
			},
			userId: {
				type: String,
				required: [true, "User Id is required"]
			},
			userEmail: {
				type: String,
				required: ["Input User Id"]
			},
			quantity: {
				type: Number,
				required: [true, "Quantity is required"]
			},
			purchasedOn: {
				type: Date,
			default: new Date()
			}
		}
	]
}) 

module.exports = mongoose.model("Product", productSchema);