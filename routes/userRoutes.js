const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController.js");
const auth = require("../auth.js");

router.post("/register", (request, response) => {
	userController.registerUser(request.body).then(resultFromControllers => response.send(resultFromControllers));
})

router.post("/checkEmail", (request, response) => {
	userController.checkEmailExist(request.body).then(resultFromControllers => response.send(resultFromControllers));
})

router.post("/retrieveDetails", (request, response) => {
	userController.retrieveUserDetails(request.body).then(resultFromControllers => response.send(resultFromControllers))
})

router.post("/login", (request, response) => {
	userController.loginUser(request.body).then(resultFromControllers => response.send(resultFromControllers))
})

// router.post("/orders", auth.verify, userController.orders);

module.exports = router;