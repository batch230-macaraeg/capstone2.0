const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
		firstName: {
			type: String,
			required: [true, "First Name is required"]
		},
		lastName: {
			type: String,
			required: [true, "Last Name is required"]
		},
		email: {
			type: String,
			required: [true, "Email is required"]
		},
		password: {
			type: String,
			required: [true, "Password is required"]
		},
		mobileNo: {
			type: String,
			required: [true, "Mobile Number is required"]
		},
		isAdmin: {
			type: Boolean,
		default: true
		},

		orders: [

		{
			 totalAmount: {
				    type: Number,
				required: [true, "Total Amount is required"]
			},
			 purchasedOn: {
				    type: Date,
				 default: new Date()
			},
			products: [

				{
					productId:{
						 type: String,
					 required: [true, "Product Id is required"]
					},
				  productName: {
						 type: String,
					 required: [true, "Product Name is required"]
					},
					 Quantity: {
						 type: Number,
					 required: [true, "Quantity is required"]
					}
				}
			]
		}
	]
})

module.exports = mongoose.model("User", userSchema);
