const mongoose = require("mongoose");
const Product = require("../models/product.js");

// Adding book Product = OK
module.exports.addProduct = (reqBody) => {

  let newProduct = new Product({
      name: reqBody.name,
      description: reqBody.description,
      price: reqBody.price,
      stocks: reqBody.stocks
  })

  return newProduct.save().then((newProduct, error) =>
  {
      if(error){
          return error;
      }
      else{
          return newProduct;
      }
  })
 }

// Retrieving all book Product = OK
module.exports.getAllProduct = () => {
    return Product.find({}).then(result => {
        return result;
    })
}

// Retrieveng ACTIVE Product = OK
module.exports.getActiveProduct = () => {
    return Product.find({isActive:true}).then(result => {
        return result;
    })
}

// Retrieving SINGLE Product = OK
module.exports.getProduct = (productId) => {
    return Product.findById(productId).then(result => {
        return result;
    })
}

// Update Product Information = OK

module.exports.updateProduct = (productId, newData) => {
    if(newData.isAdmin == true){
        return Product.findByIdAndUpdate(productId,
            {
                name: newData.product.name, 
                description: newData.product.description,
                price: newData.product.price,
                stocks: newData.product.stocks
            }
        ).then((result, error)=>{
            if(error){
                return false;
            }
            return true
        })
    }
    else{
        let message = Promise.resolve('Only Admin can access this functionality');
        return message.then((value) => {return value});
    }
}


// Archiving Product = NOT OK
    // Soft delete happens when a course status (isActive) is set to false.

module.exports.archiveProduct = (req, res) =>{

    const userData = auth.decode(req.headers.authorization);

    let updateIsActiveField = {
        isActive: req.body.isActive
    }

    if(userData.isAdmin){
        return Product.findByIdAndUpdate(req.params.productId, updateIsActiveField)
        .then(result => {
            console.log(result);
            res.send(true);
        })
        .catch(error =>{
            console.log(error);
            res.send(false);
        })
    }
    else{
        return res.send(false);
        // return res.status(401).send("You don't have access to this page!");
    }
}



// module.exports.addProduct = (reqBody, result) => {
//     console.log(result)
//     if(result.isAdmin == true){
//         let newProduct = new Product({
//             name: reqBody.name,
//             description: reqBody.description,
//             price: reqBody.price,
//             stocks: reqBody.stocks
//         })
//         return newProduct.save().then((newProduct,error) =>{

//             if(error){
//                 return false;
//             }
//             else{
//                 return newProduct;
//             }
//         })
//     }
//     else{

//         let message = Promise.resolve('ADMIN user can access this functionality');
//         return message.then((value) => {return value});
//     }
// }