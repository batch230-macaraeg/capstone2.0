const User = require("../models/user.js");
const Product = require("../models/product.js");

const bcrypt = require("bcrypt");
const auth = require("../auth.js");

module.exports.registerUser = (reqBody) => {
	let newUser = new User ({
		firstName: reqBody.firstName,
		 lastName: reqBody.lastName,
		    email: reqBody.email,
		 password: bcrypt.hashSync(reqBody.password, 10),
		 mobileNo: reqBody.mobileNo
	})

	return newUser.save().then((user, error) => {
		if(error){
			return false;
		}
		else{
			return true;
		}
	})
}

module.exports.checkEmailExist = (reqBody) => {
	return User.find({email: reqBody.email}).then(result => {
		if(result.length > 0){
			return true;
		}
		else{
			return false;
		}
	})
}

module.exports.retrieveUserDetails = (reqBody) => {
	console.log(reqBody)
	return User.findById(reqBody._id).then(result => {
		if(result == null){
			return false;
		}
		else{
			result.password = "*****";
			return result;
		}
	})

}

module.exports.loginUser = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result => {
		if(result == null){
			return false
		}
		else{
			// compareSync is a bcrypt function to compare unhashed password to hashed password
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password); //true or false

			if(isPasswordCorrect){
				// Let's give the user a token to a access features
				return {access: auth.createAccessToken(result)};
			}
			else{
				// If password does not match, else
				return false;

			}
		}
	})
}


// Create order feature
/*
module.exports.order = async (req, res) =>{

	const userData = auth.decode(req.headers.authorization);

	let productName = await Product.findById(req.body.productId).then(result => result.name);

	let newData = {
		// User ID and email will be retrieved from the request header
		userId: userData.id,
		email: userData.email,
		// Course ID will be retrieved form the request body
		productId:req.body.productId,
		// courseName value is retrieved using findById method.
		productName: productName
	}

	console.log(newData);

		// a user is updated if we receive a "true" value
	let isUserUpdated = await User.findById(newData.userId)
	.then(user =>{
		user.enrollments.push({
			productId: newData.productId,
			productName: newData.productName
		});

		// Save the updated user information in the database
		return user.save()
		.then(result =>{
			console.log(result);
			return true;
		})
		.catch(error =>{
			console.log(error);
			return false;
		})
	})

	console.log(isUserUpdated);

	let isProductUpdated = await Product.findById(newData.productId).then(product =>{

		product.enrollees.push({
			userId: newData.userId,
			email: newData.email
		})

		// Minus the slots available by 1
		product.slots -= 1;

		return product.save()
		.then(result =>{
			console.log(result);
			return true;
		})
		.catch(error =>{
			console.log(error);
			return false;
		})
	})

	console.log(isProductUpdated);
	
	(isUserUpdated && isProductUpdated) ? res.send(true) : res.send(false)

}

*/